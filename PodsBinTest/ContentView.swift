//
//  ContentView.swift
//  PodsBinTest
//
//  Created by Dev SideChef on 2020/9/18.
//  Copyright © 2020 Dev SideChef. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
